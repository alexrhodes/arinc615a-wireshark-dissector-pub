/*
    Arinc 615a Wireshark Dissector
    Copyright (C) 2019  Alex Rhodes
    https://www.alexrhodes.io

    Copied from packet-tftp.c

    Wireshark - Network traffic analyzer
    By Gerald Combs <gerald@wireshark.org>
    Copyright 2018 Gerald Combs

    SPDX-License-Identifier: GPL-3.0-or-later
*/

#include "config.h"
#include <stdlib.h>
#include <epan/packet.h>
#include <epan/reassemble.h>
#include <epan/conversation.h>

typedef int bool; /* for readability. */
#define true 1
#define false 0

/* TFTP Definitions */
#define TFTP_RRQ 1
#define TFTP_WRQ 2
#define TFTP_DATA 3
#define TFTP_ACK 4
#define TFTP_ERROR 5
#define TFTP_OACK 6
#define TFTP_INFO 255
#define TFTP_ERR_NOT_DEF 0
#define TFTP_ERR_NOT_FOUND 1
#define TFTP_ERR_NOT_ALLOWED 2
#define TFTP_ERR_DISK_FULL 3
#define TFTP_ERR_BAD_OP 4
#define TFTP_ERR_BAD_ID 5
#define TFTP_ERR_EXISTS 6
#define TFTP_ERR_NO_USER 7
#define TFTP_ERR_OPT_FAIL 8

/* Arinc 615a extension strings */
#define LCI_STR "LCI"
#define LCL_STR "LCL"
#define LCS_STR "LCS"
#define LNA_STR "LNA"
#define LND_STR "LND"
#define LNL_STR "LNL"
#define LNO_STR "LNO"
#define LNR_STR "LNR"
#define LNS_STR "LNS"
#define LUB_STR "LUB"
#define LUH_STR "LUH"
#define LUI_STR "LUI"
#define LUM_STR "LUM"
#define LUP_STR "LUP"
#define LUR_STR "LUR"
#define LUS_STR "LUS"

/* this item holds information about an on-going transfer. */
typedef struct transfer_data_t
{
    char *filename; /* the A615a file name */
    gint blocksize; /* the negotiated block size for the transfer */
} transfer_data_t;

/* this item is for configuring the server port(s) used during the capture */
static range_t *global_a615a_server_control_port_range = NULL;

/* this item is for configuring the target control port(s) used during the capture */
static range_t *global_a615a_target_control_port_range = NULL;

/* this item is the handle for the A615a dissector */
static dissector_handle_t a615a_handle;

/* this item is the Arinc 615a protocol handle */
static int proto_a615a = -1;

/* this item holds TFTP opcodes */
static const value_string tftp_opcode_vals[] =
    {
        {TFTP_RRQ, "Read Request"},
        {TFTP_WRQ, "Write Request"},
        {TFTP_DATA, "Data Packet"},
        {TFTP_ACK, "Acknowledgement"},
        {TFTP_ERROR, "Error Code: "},
        {TFTP_OACK, "Option Acknowledgement"},
        {0, NULL}};

/* this item holds TFTP error codes */
static const value_string tftp_error_code_vals[] =
    {
        {TFTP_ERR_NOT_DEF, "Not defined"},
        {TFTP_ERR_NOT_FOUND, "File not found"},
        {TFTP_ERR_NOT_ALLOWED, "Access violation"},
        {TFTP_ERR_DISK_FULL, "Disk full or allocation exceeded"},
        {TFTP_ERR_BAD_OP, "Illegal TFTP Operation"},
        {TFTP_ERR_BAD_ID, "Unknown transfer ID"},
        {TFTP_ERR_EXISTS, "File already exists"},
        {TFTP_ERR_NO_USER, "No such user"},
        {TFTP_ERR_OPT_FAIL, "Option negotiation failed"},
        {0, NULL}};

/* this item holds A615a operation status codes */
static const value_string a615a_op_status_codes[] =
    {
        {0x1, "Accepted, not yet started"},
        {0x2, "Operation in progress"},
        {0x3, "Operation completed without error"},
        {0x4, "Operation in progress, details in status description"},
        {0x1000, "Operation denied, reason in status description"},
        {0x1002, "Operation not supported by the target"},
        {0x1003, "Operation aborted by target hardware, info in status description"},
        {0x1004, "Operation aborted by target on Dataloader error message"},
        {0x1005, "Operation aborted by target on operator action"},
        {0x1007, "Load of this header file has failed, details in status description"},
        {0, NULL}};

/* this enumeration represents the A615a and A665 file types */
enum A615A_SUFFIX
{
    LCI = 0,
    LCL,
    LCS,
    LNA,
    LND,
    LNL,
    LNO,
    LNR,
    LNS,
    LUB, /* Arinc 665 */
    LUH, /* Arinc 665 */
    LUI,
    LUM, /* Arinc 665 */
    LUP, /* Arinc 665 */
    LUR,
    LUS
};


/* this item holds file extension strings */
static char a615a_file_ext[16][4] =
    {
        {LCI_STR},
        {LCL_STR},
        {LCS_STR},
        {LNA_STR},
        {LND_STR},
        {LNL_STR},
        {LNO_STR},
        {LNR_STR},
        {LNS_STR},
        {LUB_STR},
        {LUH_STR},
        {LUI_STR},
        {LUM_STR},
        {LUP_STR},
        {LUR_STR},
        {LUS_STR}
    };

/* tree entries */
static gint ett_a615a_fragment = -1;
static gint ett_a615a_fragments = -1;
static gint ett_a615a = -1;
static gint ett_a615a_opt = -1;
static gint ett_a615a_opt_root = -1;
static gint ett_a615a_protocol_root = -1;
static gint ett_a665_protocol_root = -1;

/* arinc 615a nodes */
static int hf_a615a_fragments = -1;
static int hf_a615a_fragment = -1;
static int hf_a615a_fragment_overlap = -1;
static int hf_a615a_fragment_overlap_conflicts = -1;
static int hf_a615a_fragment_multiple_tails = -1;
static int hf_a615a_fragment_too_long_fragment = -1;
static int hf_a615a_fragment_error = -1;
static int hf_a615a_fragment_count = -1;
static int hf_a615a_reassembled_in = -1;
static int hf_a615a_reassembled_length = -1;
static int hf_a615a_file_length = -1;
static int hf_a615a_protocol_version = -1;
static int hf_a615a_counter = -1;
static int hf_a615a_info_op_status = -1;
static int hf_a615a_upload_op_status = -1;
static int hf_a615a_download_op_status = -1;
static int hf_a615a_part_load_op_status = -1;
static int hf_a615a_exception_timer = -1;
static int hf_a615a_estimated_time = -1;
static int hf_a615a_status_description_length = -1;
static int hf_a615a_status_description = -1;
static int hf_a615a_load_ratio = -1;
static int hf_a615a_file_count = -1;
static int hf_a615a_file_name_length = -1;
static int hf_a615a_file_name = -1;
static int hf_a615a_file_description_length = -1;
static int hf_a615a_file_description = -1;
static int hf_a615a_part_number_length = -1;
static int hf_a615a_part_number = -1;
static int hf_a615a_tgt_hw_count = -1;
static int hf_a615a_lit_name_length = -1;
static int hf_a615a_lit_name = -1;
static int hf_a615a_serial_num_length = -1;
static int hf_a615a_serial_num = -1;
static int hf_a615a_part_num_count = -1;
static int hf_a615a_ammendment_len = -1;
static int hf_a615a_ammendment = -1;
static int hf_a615a_designation_len = -1;
static int hf_a615a_designation = -1;
static int hf_a615a_user_data_len = -1;
static int hf_a615a_user_data = -1;

/* TFTP  nodes */
static int hf_tftp_opcode = -1;
static int hf_tftp_destination_file = -1;
static int hf_tftp_source_file = -1;
static int hf_tftp_mode = -1;
static int hf_tftp_blocknum = -1;
static int hf_tftp_error_code = -1;
static int hf_tftp_ack = -1;
static int hf_tftp_option_name = -1;
static int hf_tftp_option_value = -1;


static int dissect_a615a_tftp_options(tvbuff_t *tvb, packet_info *pinfo, int offset, proto_tree *tftp_tree, transfer_data_t *tdata);
static proto_tree *dissect_a615a_header(tvbuff_t *tvb, packet_info *pinfo, int *offsetPtr, proto_tree *tftp_tree, char *file, char *ext);
static void dissect_a615a_LCL(tvbuff_t *tvb, packet_info *pinfo, int offset, proto_tree *tftp_tree);
static void dissect_a615a_LUS(tvbuff_t *tvb, packet_info *pinfo, int offset, proto_tree *tftp_tree);
static void dissect_a615a_LCS(tvbuff_t *tvb, packet_info *pinfo, int offset, proto_tree *tftp_tree);
static void dissect_a615a_LUI_Common(tvbuff_t *tvb, packet_info *pinfo, int offset, proto_tree *root);
static void dissect_a615a_LUI(tvbuff_t *tvb, packet_info *pinfo, int offset, proto_tree *tftp_tree);
static void dissect_a615a_LCI(tvbuff_t *tvb, packet_info *pinfo, int offset, proto_tree *tftp_tree);
static void dissect_a615a_LND(tvbuff_t *tvb, packet_info *pinfo, int offset, proto_tree *tftp_tree);
static void dissect_a615a_LNO(tvbuff_t *tvb, packet_info *pinfo, int offset, proto_tree *tftp_tree);
static void dissect_a615a_LUR(tvbuff_t *tvb, packet_info *pinfo, int offset, proto_tree *tftp_tree);
static void dissect_a615a_LNL(tvbuff_t *tvb, packet_info *pinfo, int offset, proto_tree *tftp_tree);
static void dissect_a615a_LNR(tvbuff_t *tvb, packet_info *pinfo, int offset, proto_tree *tftp_tree);
static void dissect_a615a_LNS(tvbuff_t *tvb, packet_info *pinfo, int offset, proto_tree *tftp_tree);
static void dissect_a615a_LNA(tvbuff_t *tvb, packet_info *pinfo, int offset, proto_tree *tftp_tree);
static void dissect_a615a_a665_msg(tvbuff_t *tb, packet_info *pinfo, int offset, const char *a665Str, proto_tree *tftp_tree);
static int dissect_a615a_protocol_file(tvbuff_t *tvb, packet_info *pinfo, int offset, proto_tree *tftp_tree, int suffix);
static int dissect_a615a(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree _U_, void *data _U_);


/* private data */
static reassembly_table a615a_reassembly_table;

/* this item holds fragment items used for reassembly */
static const fragment_items a615a_frag_items =
    {
        /* fragment subtrees */
        &ett_a615a_fragment,
        &ett_a615a_fragments,
        /* fragment fields */
        &hf_a615a_fragments,
        &hf_a615a_fragment,
        &hf_a615a_fragment_overlap,
        &hf_a615a_fragment_overlap_conflicts,
        &hf_a615a_fragment_multiple_tails,
        &hf_a615a_fragment_too_long_fragment,
        &hf_a615a_fragment_error,
        &hf_a615a_fragment_count,
        /* reassembled in field */
        &hf_a615a_reassembled_in,
        /* reassembled length field */
        &hf_a615a_reassembled_length,
        /* reassembled data field */
        NULL,
        /* tag */
        "A615a fragments"
    };

/* This routine dissects and parses TFTP options */
static int
dissect_a615a_tftp_options(tvbuff_t *tvb, packet_info *pinfo, int offset, proto_tree *tftp_tree, transfer_data_t *tdata)
{
    gint option_len, value_offset, value_len;
    const char *optionname;
    const char *optionvalue;
    proto_tree *opt_tree;
    proto_tree *opt_root;
    opt_root = proto_tree_add_subtree_format(tftp_tree, tvb, offset, tvb_captured_length_remaining(tvb, offset), ett_a615a_opt_root, NULL, "Options");
    while (tvb_offset_exists(tvb, offset))
    {
        option_len = tvb_strsize(tvb, offset);
        value_offset = offset + option_len;
        value_len = tvb_strsize(tvb, value_offset);
        optionname = tvb_format_text(tvb, offset, option_len - 1);
        optionvalue = tvb_format_text(tvb, value_offset, value_len - 1);

        opt_tree = proto_tree_add_subtree_format(opt_root, tvb, offset, option_len + value_len, ett_a615a_opt, NULL, "%s = %s", optionname, optionvalue);
        proto_tree_add_item(opt_tree, hf_tftp_option_name, tvb, offset, option_len, ENC_ASCII | ENC_NA);
        proto_tree_add_item(opt_tree, hf_tftp_option_value, tvb, value_offset, value_len, ENC_ASCII | ENC_NA);
        offset += option_len + value_len;
        if (strcmp(optionname, "blksize") == 0)
        {
            tdata->blocksize = atoi(optionvalue);
        }
    }
    return offset;
}

/* this routine dissects the common field at the top of an a615a data file */
static proto_tree *
dissect_a615a_header(tvbuff_t *tvb, packet_info *pinfo, int *offsetPtr, proto_tree *tftp_tree, char *file, char *ext)
{
    gint offset = *offsetPtr;
    proto_tree *root;
    root = proto_tree_add_subtree_format(tftp_tree, tvb, offset, -1, ett_a615a_protocol_root, NULL, "%s (%s)", file, ext);
    /* file length */
    guint32 fl = tvb_get_ntohl(tvb, offset);
    proto_tree_add_uint(root, hf_a615a_file_length, tvb, offset, 4, fl);
    offset += 4;

    /* protocol Version */
    gint end = 2;
    char *protoVersion = tvb_get_stringz_enc(wmem_packet_scope(), tvb, offset, &end, ENC_ASCII);
    proto_tree_add_string(root, hf_a615a_protocol_version, tvb, offset, 2, protoVersion);
    offset += 2;
    *offsetPtr = offset;
    return root;
}

/* this item dissects an LCL file */
static void
dissect_a615a_LCL(tvbuff_t *tvb, packet_info *pinfo, int offset, proto_tree *tftp_tree)
{
    proto_tree *root = dissect_a615a_header(tvb, pinfo, &offset, tftp_tree, "Load Configuration List", "LCL");
    gint end;

    /* target hardware count */
    guint16 numTgtHardware = tvb_get_ntohs(tvb, offset);
    proto_tree_add_uint(root, hf_a615a_tgt_hw_count, tvb, offset, 2, numTgtHardware);
    offset += 2;
    for (unsigned i = 0; i < numTgtHardware; i++)
    {
        /* literal name length */
        guint8 nameLength = tvb_get_guint8(tvb, offset);
        proto_tree_add_uint(root, hf_a615a_lit_name_length, tvb, offset, 1, nameLength);
        offset += 1;

        if (nameLength > 0)
        {
            /* literal name */
            end = nameLength;
            char *name = tvb_get_stringz_enc(wmem_packet_scope(), tvb, offset, &end, ENC_ASCII);
            proto_tree_add_string(root, hf_a615a_lit_name, tvb, offset, nameLength, name);
            offset += nameLength;
        }

        /* serial number length */
        guint8 numLength = tvb_get_guint8(tvb, offset);
        proto_tree_add_uint(root, hf_a615a_serial_num_length, tvb, offset, 1, numLength);
        offset += 1;

        if (numLength > 0)
        {
            /* serial number */
            end = numLength;
            char *serialNum = tvb_get_stringz_enc(wmem_packet_scope(), tvb, offset, &end, ENC_ASCII);
            proto_tree_add_string(root, hf_a615a_serial_num, tvb, offset, numLength, serialNum);
            offset += numLength;
        }

        /* part number count */
        guint16 numPartNumbers = tvb_get_ntohs(tvb, offset);
        proto_tree_add_uint(root, hf_a615a_part_num_count, tvb, offset, 2, numPartNumbers);
        offset += 2;

        for (unsigned i = 0; i < numPartNumbers; i++)
        {
            /* part number length */
            gint partNumLengthOffset = offset;
            guint8 partNumberLength = tvb_get_guint8(tvb, offset);

            offset += 1;

            /* part number */
            gint partNumOffset = offset;
            gint end = partNumberLength;
            char *pnum = tvb_get_stringz_enc(wmem_packet_scope(), tvb, offset, &end, ENC_ASCII);
            offset += partNumberLength;

            proto_tree *part_root;
            part_root = proto_tree_add_subtree_format(root, tvb, partNumLengthOffset, -1, ett_a615a_protocol_root, NULL, "Part %d - %s", i + 1, pnum);
            proto_tree_add_uint(part_root, hf_a615a_part_number_length, tvb, partNumLengthOffset, 1, partNumberLength);
            proto_tree_add_string(part_root, hf_a615a_part_number, tvb, partNumOffset, partNumberLength, pnum);

            /* ammendment length */
            guint8 ammendLen = tvb_get_guint8(tvb, offset);
            proto_tree_add_uint(part_root, hf_a615a_ammendment_len, tvb, offset, 1, ammendLen);
            offset += 1;

            if (ammendLen > 0)
            {
                /* ammendment */
                end = ammendLen;
                char *ammend = tvb_get_stringz_enc(wmem_packet_scope(), tvb, offset, &end, ENC_ASCII);
                proto_tree_add_string(part_root, hf_a615a_ammendment, tvb, offset, ammendLen, ammend);
                offset += ammendLen;
            }

            /* part designation length */
            guint8 desLen = tvb_get_guint8(tvb, offset);
            proto_tree_add_uint(part_root, hf_a615a_designation_len, tvb, offset, 1, desLen);
            offset += 1;

            if (desLen > 0)
            {
                /* part designation */
                end = desLen;
                char *des = tvb_get_stringz_enc(wmem_packet_scope(), tvb, offset, &end, ENC_ASCII);
                proto_tree_add_string(part_root, hf_a615a_designation, tvb, offset, desLen, des);
                offset += desLen;
            }
        }
    }
}

/* This item dissects an LUS file */
static void
dissect_a615a_LUS(tvbuff_t *tvb, packet_info *pinfo, int offset, proto_tree *tftp_tree)
{
    gint end;
    proto_tree *root = dissect_a615a_header(tvb, pinfo, &offset, tftp_tree, "Load Upload Status", "LUS");

    /* upload op status code */
    guint16 opCode = tvb_get_ntohs(tvb, offset);
    proto_tree_add_uint(root, hf_a615a_upload_op_status, tvb, offset, 2, opCode);
    offset += 2;

    /* status length */
    guint8 statLength = tvb_get_guint8(tvb, offset);
    proto_tree_add_uint(root, hf_a615a_status_description_length, tvb, offset, 1, statLength);
    offset += 1;

    if (statLength > 0)
    {
        /* status description */
        end = statLength;
        char *statDesc = tvb_get_stringz_enc(wmem_packet_scope(), tvb, offset, &end, ENC_ASCII);
        proto_tree_add_string(root, hf_a615a_status_description, tvb, offset, statLength, statDesc);
        offset += statLength;
    }

    /* counter */
    guint16 counter = tvb_get_ntohs(tvb, offset);
    proto_tree_add_uint(root, hf_a615a_counter, tvb, offset, 2, counter);
    offset += 2;

    /* exception timer */
    guint16 excTimer = tvb_get_ntohs(tvb, offset);
    proto_tree_add_uint(root, hf_a615a_exception_timer, tvb, offset, 2, excTimer);
    offset += 2;

    /* estimated time */
    guint16 estTime = tvb_get_ntohs(tvb, offset);
    proto_tree_add_uint(root, hf_a615a_estimated_time, tvb, offset, 2, estTime);
    offset += 2;

    /* load list ratio */
    end = 3;
    char *llr = tvb_get_stringz_enc(wmem_packet_scope(), tvb, offset, &end, ENC_ASCII);
    proto_tree_add_string(root, hf_a615a_load_ratio, tvb, offset, 3, llr);
    col_append_fstr(pinfo->cinfo, COL_INFO, ", Status: %s", val_to_str(opCode, a615a_op_status_codes, "Unknown (0x%04x)"));
    col_append_fstr(pinfo->cinfo, COL_INFO, ", Load Ratio: %s", llr);

    offset += 3;

    /* header file count */
    guint16 count = tvb_get_ntohs(tvb, offset);
    proto_tree_add_uint(root, hf_a615a_file_count, tvb, offset, 2, count);
    offset += 2;

    for (unsigned i = 0; i < count; i++)
    {
        /* file name length */
         gint fnameLengthOffset = offset;
        guint8 fnameLength = tvb_get_guint8(tvb, fnameLengthOffset);
        offset += 1;

        /* file name */
        gint fnameOffset = offset;
        end = fnameLength;
        char *fname = tvb_get_stringz_enc(wmem_packet_scope(), tvb, fnameOffset, &end, ENC_ASCII);
        offset += fnameLength;

        proto_tree *part_root;
        part_root = proto_tree_add_subtree_format(root, tvb, fnameLengthOffset, -1, ett_a615a_protocol_root, NULL, "Header %d - %s", i + 1, fname);

        proto_tree_add_uint(part_root, hf_a615a_file_name_length, tvb, fnameLengthOffset, 1, fnameLength);
        proto_tree_add_string(part_root, hf_a615a_file_name, tvb, fnameOffset, fnameLength, fname);

        /* part number length */
        guint8 partNumberLength = tvb_get_guint8(tvb, offset);
        proto_tree_add_uint(part_root, hf_a615a_part_number_length, tvb, offset, 1, partNumberLength);
        offset += 1;

        if (partNumberLength > 0)
        {
            /* part number */
            gint end = partNumberLength;
            char *pnum = tvb_get_stringz_enc(wmem_packet_scope(), tvb, offset, &end, ENC_ASCII);
            proto_tree_add_string(part_root, hf_a615a_part_number, tvb, offset, partNumberLength, pnum);
            offset += partNumberLength;
        }

        /* load ratio */
        end = 3;
        char *llr = tvb_get_stringz_enc(wmem_packet_scope(), tvb, offset, &end, ENC_ASCII);
        proto_tree_add_string(part_root, hf_a615a_load_ratio, tvb, offset, 3, llr);
        offset += 3;

        /* soad status code */
        opCode = tvb_get_ntohs(tvb, offset);
        proto_tree_add_uint(part_root, hf_a615a_part_load_op_status, tvb, offset, 2, opCode);
        offset += 2;

        /* status length */
        statLength = tvb_get_guint8(tvb, offset);
        proto_tree_add_uint(part_root, hf_a615a_status_description_length, tvb, offset, 1, statLength);
        offset += 1;

        if (statLength > 0)
        {
            /* status description */
            end = statLength;
            char *statDesc = tvb_get_stringz_enc(wmem_packet_scope(), tvb, offset, &end, ENC_ASCII);
            proto_tree_add_string(part_root, hf_a615a_status_description, tvb, offset, tvb_captured_length_remaining(tvb, offset), statDesc);
            offset += statLength;
        }
    }
}

/* this routine dissects an LCS file */
static void
dissect_a615a_LCS(tvbuff_t *tvb, packet_info *pinfo, int offset, proto_tree *tftp_tree)
{
    proto_tree *root = dissect_a615a_header(tvb, pinfo, &offset, tftp_tree, "Load Control Status", "LCS");
    /* counter */
    guint16 counter = tvb_get_ntohs(tvb, offset);
    proto_tree_add_uint(root, hf_a615a_counter, tvb, offset, 2, counter);
    offset += 2;

    /* info op status code */
    guint16 opCode = tvb_get_ntohs(tvb, offset);
    proto_tree_add_uint(root, hf_a615a_info_op_status, tvb, offset, 2, opCode);
    col_append_fstr(pinfo->cinfo, COL_INFO, ", Status: %s", val_to_str(opCode, a615a_op_status_codes, "Unknown (0x%04x)"));
    offset += 2;

    /* exception timer */
    guint16 excTimer = tvb_get_ntohs(tvb, offset);
    proto_tree_add_uint(root, hf_a615a_exception_timer, tvb, offset, 2, excTimer);
    offset += 2;

    /* estimated time */
    guint16 estTime = tvb_get_ntohs(tvb, offset);
    proto_tree_add_uint(root, hf_a615a_estimated_time, tvb, offset, 2, estTime);
    offset += 2;

    /* status length */
    guint8 statLength = tvb_get_guint8(tvb, offset);
    proto_tree_add_uint(root, hf_a615a_status_description_length, tvb, offset, 1, statLength);
    offset += 1;

    if (statLength > 0)
    {
        /* status description */
        gint end = statLength;
        char *statDesc = tvb_get_stringz_enc(wmem_packet_scope(), tvb, offset, &end, ENC_ASCII);
        proto_tree_add_string(root, hf_a615a_status_description, tvb, offset, tvb_captured_length_remaining(tvb, offset), statDesc);
        offset += end;
    }
}

/* this routine dissects LUI, LCI, LND, and LNO files */
static void
dissect_a615a_LUI_Common(tvbuff_t *tvb, packet_info *pinfo, int offset, proto_tree *root)
{
    /* op status code */
    guint16 opCode = tvb_get_ntohs(tvb, offset);
    proto_tree_add_uint(root, hf_a615a_upload_op_status, tvb, offset, 2, opCode);
    col_append_fstr(pinfo->cinfo, COL_INFO, ", Status: %s", val_to_str(opCode, a615a_op_status_codes, "Unknown (0x%04x)"));
    offset += 2;

    /* status length */
    guint8 statLength = tvb_get_guint8(tvb, offset);
    proto_tree_add_uint(root, hf_a615a_status_description_length, tvb, offset, 1, statLength);
    offset += 1;

    if (statLength > 0)
    {
        /* Status description */
        gint end = statLength;
        char *statDesc = tvb_get_stringz_enc(wmem_packet_scope(), tvb, offset, &end, ENC_ASCII);
        proto_tree_add_string(root, hf_a615a_status_description, tvb, offset, tvb_captured_length_remaining(tvb, offset), statDesc);
        offset += end;
    }
}

/* this routine dissects an LUI file */
static void
dissect_a615a_LUI(tvbuff_t *tvb, packet_info *pinfo, int offset, proto_tree *tftp_tree)
{
    proto_tree *root = dissect_a615a_header(tvb, pinfo, &offset, tftp_tree, "Load Upload Initialization", "LUI");
    dissect_a615a_LUI_Common(tvb, pinfo, offset, root);
}

/* this routine diessects an LCI file */
static void
dissect_a615a_LCI(tvbuff_t *tvb, packet_info *pinfo, int offset, proto_tree *tftp_tree)
{
    proto_tree *root = dissect_a615a_header(tvb, pinfo, &offset, tftp_tree, "Load Configuration Initialization", "LCI");
    dissect_a615a_LUI_Common(tvb, pinfo, offset, root);
}

/* this routine dissects an LND file */
static void
dissect_a615a_LND(tvbuff_t *tvb, packet_info *pinfo, int offset, proto_tree *tftp_tree)
{
    proto_tree *root = dissect_a615a_header(tvb, pinfo, &offset, tftp_tree, "Load Downloading Media", "LND");
    dissect_a615a_LUI_Common(tvb, pinfo, offset, root);
}

/* this routine dissects an LNO file */
static void
dissect_a615a_LNO(tvbuff_t *tvb, packet_info *pinfo, int offset, proto_tree *tftp_tree)
{
    proto_tree *root = dissect_a615a_header(tvb, pinfo, &offset, tftp_tree, "Load Downloading Operator", "LNO");
    dissect_a615a_LUI_Common(tvb, pinfo, offset, root);
}

/* this routine diessects an LUR file */
static void
dissect_a615a_LUR(tvbuff_t *tvb, packet_info *pinfo, int offset, proto_tree *tftp_tree)
{
    gint end = -1;
    proto_tree *root = dissect_a615a_header(tvb, pinfo, &offset, tftp_tree, "Load Upload Request", "LUR");

    /* Header file count */
    guint16 count = tvb_get_ntohs(tvb, offset);
    proto_tree_add_uint(root, hf_a615a_file_count, tvb, offset, 2, count);
    offset += 2;

    for (unsigned i = 0; i < count; i++)
    {
        /* file name length */
        gint fnameLengthOffset = offset;
        guint8 fnameLength = tvb_get_guint8(tvb, fnameLengthOffset);
        offset += 1;
        /* file name */
        gint fnameOffset = offset;
        end = fnameLength;
        char *fname = tvb_get_stringz_enc(wmem_packet_scope(), tvb, fnameOffset, &end, ENC_ASCII);
        offset += fnameLength;

        proto_tree *part_root;
        part_root = proto_tree_add_subtree_format(root, tvb, fnameLengthOffset, -1, ett_a615a_protocol_root, NULL, "Header %d - %s", i + 1, fname);

        proto_tree_add_uint(part_root, hf_a615a_file_name_length, tvb, fnameLengthOffset, 1, fnameLength);
        proto_tree_add_string(part_root, hf_a615a_file_name, tvb, fnameOffset, fnameLength, fname);

        /* part number length */
        guint8 partNumberLength = tvb_get_guint8(tvb, offset);
        proto_tree_add_uint(part_root, hf_a615a_part_number_length, tvb, offset, 1, partNumberLength);
        offset += 1;

        if (partNumberLength > 0)
        {
            /* Part number */
            gint end = partNumberLength;
            char *pnum = tvb_get_stringz_enc(wmem_packet_scope(), tvb, offset, &end, ENC_ASCII);
            proto_tree_add_string(part_root, hf_a615a_part_number, tvb, offset, partNumberLength, pnum);
            offset += partNumberLength;
        }
    }
}

/* this routine dissects an LNL file */
static void
dissect_a615a_LNL(tvbuff_t *tvb, packet_info *pinfo, int offset, proto_tree *tftp_tree)
{
    gint end = -1;
    proto_tree *root = dissect_a615a_header(tvb, pinfo, &offset, tftp_tree, "Load Downloading List", "LNL");

    /* file count */
    guint16 count = tvb_get_ntohs(tvb, offset);
    proto_tree_add_uint(root, hf_a615a_file_count, tvb, offset, 2, count);
    offset += 2;

    for (unsigned i = 0; i < count; i++)
    {
        /* file name length */
        gint fnameLengthOffset = offset;
        guint8 fnameLength = tvb_get_guint8(tvb, fnameLengthOffset);
        offset += 1;

        /* file name */
        gint fnameOffset = offset;
        end = fnameLength;
        char *fname = tvb_get_stringz_enc(wmem_packet_scope(), tvb, fnameOffset, &end, ENC_ASCII);
        offset += fnameLength;

        proto_tree *part_root;
        part_root = proto_tree_add_subtree_format(root, tvb, fnameLengthOffset, -1, ett_a615a_protocol_root, NULL, "Header %d - %s", i + 1, fname);

        proto_tree_add_uint(part_root, hf_a615a_file_name_length, tvb, fnameLengthOffset, 1, fnameLength);
        proto_tree_add_string(part_root, hf_a615a_file_name, tvb, fnameOffset, fnameLength, fname);

        /* file description length */
        guint8 descLength = tvb_get_guint8(tvb, offset);
        proto_tree_add_uint(part_root, hf_a615a_file_description_length, tvb, offset, 1, descLength);
        offset += 1;

        if (descLength > 0)
        {
            /* file description */
            gint end = descLength;
            char *desc = tvb_get_stringz_enc(wmem_packet_scope(), tvb, offset, &end, ENC_ASCII);
            proto_tree_add_string(part_root, hf_a615a_file_description, tvb, offset, descLength, desc);
            offset += descLength;
        }
    }
}

/* this routine dissects an LNR file */
static void
dissect_a615a_LNR(tvbuff_t *tvb, packet_info *pinfo, int offset, proto_tree *tftp_tree)
{
    gint end = -1;
    proto_tree *root = dissect_a615a_header(tvb, pinfo, &offset, tftp_tree, "Load Downloading Request", "LNR");

    /* file count */
    guint16 count = tvb_get_ntohs(tvb, offset);
    proto_tree_add_uint(root, hf_a615a_file_count, tvb, offset, 2, count);
    offset += 2;

    for (unsigned i = 0; i < count; i++)
    {
        /* file name length */
        gint fnameLengthOffset = offset;
        guint8 fnameLength = tvb_get_guint8(tvb, fnameLengthOffset);
        offset += 1;
        
        /* file name */
        gint fnameOffset = offset;
        end = fnameLength;
        char *fname = tvb_get_stringz_enc(wmem_packet_scope(), tvb, fnameOffset, &end, ENC_ASCII);
        offset += fnameLength;

        proto_tree *part_root;
        part_root = proto_tree_add_subtree_format(root, tvb, fnameLengthOffset, -1, ett_a615a_protocol_root, NULL, "Header %d - %s", i + 1, fname);

        proto_tree_add_uint(part_root, hf_a615a_file_name_length, tvb, fnameLengthOffset, 1, fnameLength);
        proto_tree_add_string(part_root, hf_a615a_file_name, tvb, fnameOffset, fnameLength, fname);
    }

    /* user data length */
    guint8 userDataLen = tvb_get_guint8(tvb, offset);
    proto_tree_add_uint(root, hf_a615a_user_data_len, tvb, offset, 1, count);
    offset += 1;

    /* user data */
    proto_tree_add_item(root, hf_a615a_user_data, tvb, offset, userDataLen, ENC_NA);
}

/* this routine dissects an LNS file */
static void
dissect_a615a_LNS(tvbuff_t *tvb, packet_info *pinfo, int offset, proto_tree *tftp_tree)
{
    gint end;
    proto_tree *root = dissect_a615a_header(tvb, pinfo, &offset, tftp_tree, "Load Downloading Status", "LNS");

    /* download op status code */
    guint16 opCode = tvb_get_ntohs(tvb, offset);
    proto_tree_add_uint(root, hf_a615a_upload_op_status, tvb, offset, 2, opCode);
    offset += 2;

    /* status length */
    guint8 statLength = tvb_get_guint8(tvb, offset);
    proto_tree_add_uint(root, hf_a615a_status_description_length, tvb, offset, 1, statLength);
    offset += 1;

    if (statLength > 0)
    {
        /* status description */
        end = statLength;
        char *statDesc = tvb_get_stringz_enc(wmem_packet_scope(), tvb, offset, &end, ENC_ASCII);
        proto_tree_add_string(root, hf_a615a_status_description, tvb, offset, statLength, statDesc);
        offset += statLength;
    }

    /* counter */
    guint16 counter = tvb_get_ntohs(tvb, offset);
    proto_tree_add_uint(root, hf_a615a_counter, tvb, offset, 2, counter);
    offset += 2;

    /* exception timer */
    guint16 excTimer = tvb_get_ntohs(tvb, offset);
    proto_tree_add_uint(root, hf_a615a_exception_timer, tvb, offset, 2, excTimer);
    offset += 2;

    /* estimated time */
    guint16 estTime = tvb_get_ntohs(tvb, offset);
    proto_tree_add_uint(root, hf_a615a_estimated_time, tvb, offset, 2, estTime);
    offset += 2;

    /* load list ratio */
    end = 3;
    char *llr = tvb_get_stringz_enc(wmem_packet_scope(), tvb, offset, &end, ENC_ASCII);
    proto_tree_add_string(root, hf_a615a_load_ratio, tvb, offset, 3, llr);
    col_append_fstr(pinfo->cinfo, COL_INFO, ", Status: %s", val_to_str(opCode, a615a_op_status_codes, "Unknown (0x%04x)"));
    col_append_fstr(pinfo->cinfo, COL_INFO, ", Download Ratio: %s", llr);

    offset += 3;

    /* file count */
    guint16 count = tvb_get_ntohs(tvb, offset);
    proto_tree_add_uint(root, hf_a615a_file_count, tvb, offset, 2, count);
    offset += 2;

    for (unsigned i = 0; i < count; i++)
    {
        /* file name length */
        gint fnameLengthOffset = offset;
        guint8 fnameLength = tvb_get_guint8(tvb, fnameLengthOffset);
        offset += 1;
        
        /* file name */
        gint fnameOffset = offset;
        end = fnameLength;
        char *fname = tvb_get_stringz_enc(wmem_packet_scope(), tvb, fnameOffset, &end, ENC_ASCII);
        offset += fnameLength;

        proto_tree *part_root;
        part_root = proto_tree_add_subtree_format(root, tvb, fnameLengthOffset, -1, ett_a615a_protocol_root, NULL, "Header %d - %s", i + 1, fname);

        proto_tree_add_uint(part_root, hf_a615a_file_name_length, tvb, fnameLengthOffset, 1, fnameLength);
        proto_tree_add_string(part_root, hf_a615a_file_name, tvb, fnameOffset, fnameLength, fname);

        /* download op status code */
        guint16 opCode = tvb_get_ntohs(tvb, offset);
        proto_tree_add_uint(root, hf_a615a_download_op_status, tvb, offset, 2, opCode);
        offset += 2;

        /* file description length */
        guint8 descLength = tvb_get_guint8(tvb, offset);
        proto_tree_add_uint(part_root, hf_a615a_file_description_length, tvb, offset, 1, descLength);
        offset += 1;

        if (descLength > 0)
        {
            /* file description */
            gint end = descLength;
            char *desc = tvb_get_stringz_enc(wmem_packet_scope(), tvb, offset, &end, ENC_ASCII);
            proto_tree_add_string(part_root, hf_a615a_file_description, tvb, offset, descLength, desc);
            offset += descLength;
        }
    }
}

/* this routine dissects an LNA file */
static void
dissect_a615a_LNA(tvbuff_t *tvb, packet_info *pinfo, int offset, proto_tree *tftp_tree)
{
    gint end = -1;
    proto_tree *root = dissect_a615a_header(tvb, pinfo, &offset, tftp_tree, "Load Downloading Answer", "LNR");

    /* file count */
    guint16 count = tvb_get_ntohs(tvb, offset);
    proto_tree_add_uint(root, hf_a615a_file_count, tvb, offset, 2, count);
    offset += 2;

    for (unsigned i = 0; i < count; i++)
    {
        /* file name length */
        gint fnameLengthOffset = offset;
        guint8 fnameLength = tvb_get_guint8(tvb, fnameLengthOffset);
        offset += 1;
        
        /* file name */
        gint fnameOffset = offset;
        end = fnameLength;
        char *fname = tvb_get_stringz_enc(wmem_packet_scope(), tvb, fnameOffset, &end, ENC_ASCII);
        offset += fnameLength;

        proto_tree *part_root;
        part_root = proto_tree_add_subtree_format(root, tvb, fnameLengthOffset, -1, ett_a615a_protocol_root, NULL, "Header %d - %s", i + 1, fname);

        proto_tree_add_uint(part_root, hf_a615a_file_name_length, tvb, fnameLengthOffset, 1, fnameLength);
        proto_tree_add_string(part_root, hf_a615a_file_name, tvb, fnameOffset, fnameLength, fname);
    }
}

/* this routine provides a descriptive subtree for A665 file types */
static void
dissect_a615a_a665_msg(tvbuff_t *tvb, packet_info *pinfo, int offset, const char *a665Str, proto_tree *tftp_tree)
{
    (void)proto_tree_add_subtree_format(tftp_tree, tvb, offset, -1, ett_a665_protocol_root, NULL, "A665 %s File", a665Str);
}

/* this routien selects a dissection routine based on the file type to be dissected */
static gint
dissect_a615a_protocol_file(tvbuff_t *tvb, packet_info *pinfo, int offset, proto_tree *tftp_tree, int suffix)
{
    switch (suffix)
    {
        case LCI:
        {
            dissect_a615a_LCI(tvb, pinfo, offset, tftp_tree);
            break;
        }
        case LCL:
        {
            dissect_a615a_LCL(tvb, pinfo, offset, tftp_tree);
            break;
        }
        case LCS:
        {
            dissect_a615a_LCS(tvb, pinfo, offset, tftp_tree);
            break;
        }
        case LNA:
        {
            dissect_a615a_LNA(tvb, pinfo, offset, tftp_tree);
            break;
        }
        case LND:
        {
            dissect_a615a_LND(tvb, pinfo, offset, tftp_tree);
            break;
        }
        case LNL:
        {
            dissect_a615a_LNL(tvb, pinfo, offset, tftp_tree);
            break;
        }
        case LNO:
        {
            dissect_a615a_LNO(tvb, pinfo, offset, tftp_tree);
            break;
        }
        case LNR:
        {
            dissect_a615a_LNR(tvb, pinfo, offset, tftp_tree);
            break;
        }
        case LNS:
        {
            dissect_a615a_LNS(tvb, pinfo, offset, tftp_tree);
            break;
        }
        case LUI:
        {
            dissect_a615a_LUI(tvb, pinfo, offset, tftp_tree);
            break;
        }
        case LUR:
        {
            dissect_a615a_LUR(tvb, pinfo, offset, tftp_tree);
            break;
        }
        case LUS:
        {
            dissect_a615a_LUS(tvb, pinfo, offset, tftp_tree);
            break;
        }
        case LUB:
        {
            dissect_a615a_a665_msg(tvb, pinfo, offset, "Load Upload Batch (LUB)", tftp_tree);
            break;
        }
        case LUM:
        {
            dissect_a615a_a665_msg(tvb, pinfo, offset, "Load Upload Media (LUM)", tftp_tree);
            break;
        }
        case LUP:
        {
            dissect_a615a_a665_msg(tvb, pinfo, offset, "Load Upload Part (LUP, Data File)", tftp_tree);
            break;
        }
        case LUH:
        {
            dissect_a615a_a665_msg(tvb, pinfo, offset, "Load Upload Header (LUH)", tftp_tree);
            break;
        }
        default:
        {
            break;
        }
    }
    return tvb_captured_length(tvb);
}

/* this routine is the main dissection routine for the a615a dissector */
static int
dissect_a615a(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, void* data)
{
    conversation_t *conversation = NULL;
    transfer_data_t *tdata = NULL;

    /* look for an existing conversation between these potentially ephemeral ports */
    conversation = find_conversation(pinfo->num, &pinfo->src, &pinfo->dst, ENDPOINT_UDP, pinfo->srcport, pinfo->destport, 0);

    if ((conversation == NULL) || (conversation_get_dissector(conversation, pinfo->num) != a615a_handle))
    {
        /* the conversation wasn't found */
        if (value_is_in_range(global_a615a_target_control_port_range, pinfo->destport) ||
            value_is_in_range(global_a615a_server_control_port_range, pinfo->destport))
        {
            /* create a conversation with the source IP, dest IP, ephemeral source port, and a wildcard to repalce the loader or control port */
            conversation = conversation_new(pinfo->num, &pinfo->src, &pinfo->dst, ENDPOINT_UDP, pinfo->srcport, 0, NO_PORT2);

            /* set the dissector of this conversation */
            conversation_set_dissector(conversation, a615a_handle);
        }
        else
        {
            /* find the conversation that started with the wildcard in place of the control or loader port, and replace that with the ephemeral
               port in the response. */

            conversation = find_conversation(pinfo->num, &pinfo->dst, &pinfo->src, ENDPOINT_UDP, pinfo->destport, pinfo->srcport, NO_PORT2);

            if (conversation == NULL)
            {
                /* error case */
                return 0;
            }
        }
    }

    /* get the transfer data for this conversation */
    tdata = (transfer_data_t *)conversation_get_proto_data(conversation, proto_a615a);

    /* if the transfer data doesn't exist yet, create it and add it to the conversation */
    if (!tdata)
    {
        tdata = wmem_new(wmem_file_scope(), transfer_data_t);
        tdata->blocksize = 512; /* TFTP default block size */
        tdata->filename = NULL;
        conversation_add_proto_data(conversation, proto_a615a, tdata);
    }

    gint offset = 0;
    col_set_str(pinfo->cinfo, COL_PROTOCOL, "A615a");
    col_clear(pinfo->cinfo, COL_INFO);

    proto_item *ti = proto_tree_add_item(tree, proto_a615a, tvb, offset, -1, ENC_NA);

    proto_tree *tftp_tree = proto_item_add_subtree(ti, ett_a615a);

    /* get the opcode */
    short opcode = tvb_get_ntohs(tvb, offset);
    proto_tree_add_uint(tftp_tree, hf_tftp_opcode, tvb, offset, 2, opcode);
    col_add_str(pinfo->cinfo, COL_INFO, val_to_str(opcode, tftp_opcode_vals, "Unknown (0x%04x)"));
    offset += 2;

    /* handle the packet based on its TFTP opcode */
    switch (opcode)
    {
        case TFTP_RRQ:
        {
            gint end = -1;
            /* filename */
            char *filename = tvb_get_stringz_enc(wmem_file_scope(), tvb, offset, &end, ENC_ASCII);
            proto_tree_add_string(tftp_tree, hf_tftp_source_file, tvb, offset, end, filename);
            col_append_fstr(pinfo->cinfo, COL_INFO, ", %s", filename);
            tdata->filename = filename;
            offset += end;
            
            /* mode */
            char *modeStr = tvb_get_stringz_enc(wmem_packet_scope(), tvb, offset, &end, ENC_ASCII);
            proto_tree_add_string(tftp_tree, hf_tftp_mode, tvb, offset, end, modeStr);
            offset += end;
            if(tvb_captured_length_remaining(tvb, offset) > 0)
            {
                offset = dissect_a615a_tftp_options(tvb, pinfo, offset, tftp_tree, tdata);
            }
            break;
        }
        case TFTP_WRQ:
        {
            /* filename */
            gint end = -1;
            char *filename = tvb_get_stringz_enc(wmem_file_scope(), tvb, offset, &end, ENC_ASCII);
            proto_tree_add_string(tftp_tree, hf_tftp_destination_file, tvb, offset, end, filename);
            col_append_fstr(pinfo->cinfo, COL_INFO, ", %s", filename);
            tdata->filename = filename;
            offset += end;

            /* mode */
            end = -1;
            char *modeStr = tvb_get_stringz_enc(wmem_packet_scope(), tvb, offset, &end, ENC_ASCII);
            proto_tree_add_string(tftp_tree, hf_tftp_mode, tvb, offset, end, modeStr);
            offset += end;
            if(tvb_captured_length_remaining(tvb, offset) > 0)
            {
                offset = dissect_a615a_tftp_options(tvb, pinfo, offset, tftp_tree, tdata);
            }
            break;
        }
        case TFTP_DATA:
        {
            guint16 blocknum = tvb_get_ntohs(tvb, offset);
            proto_tree_add_uint(tftp_tree, hf_tftp_blocknum, tvb, offset, 2, blocknum);
            col_append_fstr(pinfo->cinfo, COL_INFO, ", Block number: %d", blocknum);
            offset += 2;
            gint rem = tvb_captured_length_remaining(tvb, offset);
            
            /* fragmentation stuff */
            char *filename = tdata->filename;
            bool fragmented = true;
            bool reassembled = false;
            
            /* block number is 1, and the length is less than block size, we have the whole file in one block = not fragmented*/
            if (blocknum == 1 && rem < tdata->blocksize)
            {
                fragmented = false;
                reassembled = true;
            }
            guint8 save_fragmented = pinfo->fragmented;
            if (fragmented)
            { 
                /* the file is fragmented */
                tvbuff_t *new_tvb = NULL;
                fragment_head *frag_msg = NULL;
                pinfo->fragmented = true;
                gboolean moreFrags = (rem == tdata->blocksize);
                frag_msg = fragment_add_seq_next(&a615a_reassembly_table, tvb, offset, pinfo, blocknum, NULL, rem, moreFrags);
                new_tvb = process_reassembled_data(tvb, offset, pinfo, "A615a Protocol File", frag_msg, &a615a_frag_items, NULL, tftp_tree);
                if (new_tvb)
                {
                    tvb = new_tvb;
                    offset = 0;
                    col_append_str(pinfo->cinfo, COL_INFO, ", Complete Protocol File");
                    reassembled = true;
                }
                else
                {
                    col_append_str(pinfo->cinfo, COL_INFO, ", Protocol File Fragment");
                    reassembled = false;
                }
            }

            pinfo->fragmented = save_fragmented;

            if (filename == NULL)
            {
                /* transfer data for this conversation never established the file name */
                col_append_fstr(pinfo->cinfo, COL_INFO, ", Incomplete file.");
            }
            else
            {
                /* handle parsing the file based on it's type */
                gint suffix = LCI;
                for (suffix = LCI; suffix <= LUS; suffix++)
                {
                    char *extension = a615a_file_ext[suffix];
                    if (g_str_has_suffix(filename, extension))
                    {
                        col_append_fstr(pinfo->cinfo, COL_INFO, ", %s (%s)", filename, extension);
                        break;
                    }
                }
                if (reassembled)
                {
                    offset = dissect_a615a_protocol_file(tvb, pinfo, offset, tftp_tree, suffix);
                }
            }
            break;
        }
        case TFTP_ACK:
        {
            guint16 blocknum = tvb_get_ntohs(tvb, offset);
            proto_tree_add_uint(tftp_tree, hf_tftp_ack, tvb, offset, 2, blocknum);
            col_append_fstr(pinfo->cinfo, COL_INFO, ", Block number: %d", blocknum);
            break;
        }
        case TFTP_ERROR:
        {
            short errorCode = tvb_get_ntohs(tvb, offset);
            col_append_fstr(pinfo->cinfo, COL_INFO, val_to_str(errorCode, tftp_error_code_vals, "Unknown (0x%04x)"));
            proto_tree_add_uint(tftp_tree, hf_tftp_error_code, tvb, offset, 2, errorCode);
            offset += 2;
            break;
        }
        case TFTP_OACK:
        {
            if(tvb_captured_length_remaining(tvb, offset) > 0)
            {
                offset = dissect_a615a_tftp_options(tvb, pinfo, offset, tftp_tree, tdata);
            }
            break;
        }
    }

    return offset;
}

void proto_register_a615a(void)
{
    static hf_register_info hf[] = {
        /* TFTP fields */
        {&hf_tftp_opcode,
         {"TFTP Opcode",
          "a615a.tftp.opcode",
          FT_UINT16,
          BASE_DEC,
          VALS(tftp_opcode_vals),
          0x0,
          "TFTP message type",
          HFILL}},
        {&hf_tftp_source_file,
         {"Source File Name",
          "a615a.tftp.source_file",
          FT_STRINGZ,
          BASE_NONE,
          NULL,
          0x0,
          "TFTP source file name",
          HFILL}},
        {&hf_tftp_destination_file,
         {"Destination File Name",
          "a615a.tftp.destination_file",
          FT_STRINGZ,
          BASE_NONE,
          NULL,
          0x0,
          "TFTP destination file name",
          HFILL}},
        {&hf_tftp_mode,
         {"TFTP Mode",
          "a615a.tftp.mode",
          FT_STRINGZ,
          BASE_NONE,
          NULL,
          0x0,
          "TFTP data mode",
          HFILL}},
        {&hf_tftp_blocknum,
         {"Block Number",
          "a615a.tftp.block_num",
          FT_UINT16,
          BASE_DEC,
          NULL,
          0x0,
          "TFTP Block Number",
          HFILL}},
        {&hf_tftp_ack,
         {"Ack Block Number",
          "a615a.tftp.block_num",
          FT_UINT16,
          BASE_DEC,
          NULL,
          0x0,
          "TFTP Ack Block Number",
          HFILL}},
        {&hf_tftp_error_code,
         {"Error Code",
          "a615a.tftp.error_code",
          FT_UINT16,
          BASE_DEC,
          VALS(tftp_error_code_vals),
          0x0,
          "TFTP Error Code",
          HFILL}},
        {&hf_tftp_option_name,
         {"Option name",
          "a615a.tftp.option.name",
          FT_STRINGZ,
          BASE_NONE,
          NULL,
          0x0,
          NULL,
          HFILL}},

        {&hf_tftp_option_value,
         {"Option value",
          "a615a.tftp.option.value",
          FT_STRINGZ,
          BASE_NONE,
          NULL,
          0x0,
          NULL,
          HFILL}},

        /* A615a protocol fields */
        {&hf_a615a_file_length,
         {"File Length",
          "a615a.file_length",
          FT_UINT32,
          BASE_DEC,
          NULL,
          0x0,
          "A615a Protocol File Length",
          HFILL}},
        {&hf_a615a_protocol_version,
         {"Protocol Version",
          "a615a.protocol_version",
          FT_STRINGZ,
          BASE_NONE,
          NULL,
          0x0,
          "A615a Protocol File Version",
          HFILL}},
        {&hf_a615a_counter,
         {"Counter",
          "a615a.counter",
          FT_UINT16,
          BASE_DEC,
          NULL,
          0x0,
          "A615a Protocol Counter",
          HFILL}},
        {&hf_a615a_info_op_status,
         {"Info Op Status Code",
          "a615a.info.status_code",
          FT_UINT16,
          BASE_DEC,
          VALS(a615a_op_status_codes),
          0x0,
          "A615a Information Operation Status Code",
          HFILL}},
        {&hf_a615a_exception_timer,
         {"Exception Timer",
          "a615a.exception_timer",
          FT_UINT16,
          BASE_DEC,
          NULL,
          0x0,
          "A615a Exception Timer",
          HFILL}},
        {&hf_a615a_estimated_time,
         {"Estimated Time (seconds)",
          "a615a.estimated_time",
          FT_UINT16,
          BASE_DEC,
          NULL,
          0x0,
          "A615a Estimated Time (Seconds)",
          HFILL}},
        {&hf_a615a_status_description_length,
         {"Status Length",
          "a615a.status.length",
          FT_UINT8,
          BASE_DEC,
          NULL,
          0x0,
          "A615a Status Description Length",
          HFILL}},
        {&hf_a615a_status_description,
         {"Status Description",
          "a615a.status",
          FT_STRINGZ,
          BASE_NONE,
          NULL,
          0x0,
          "A615a Status Description",
          HFILL}},

        /* load list */
        {&hf_a615a_upload_op_status,
         {"Upload Op Status Code",
          "a615a.upload.status_code",
          FT_UINT16,
          BASE_DEC,
          VALS(a615a_op_status_codes),
          0x0,
          "A615a Upload Operation Status Code",
          HFILL}},
        {&hf_a615a_download_op_status,
         {"Download Op Status Code",
          "a615a.download.status_code",
          FT_UINT16,
          BASE_DEC,
          VALS(a615a_op_status_codes),
          0x0,
          "A615a Download Operation Status Code",
          HFILL}},

        /* load target */
        {&hf_a615a_part_load_op_status,
         {"Part Load Op Status Code",
          "a615a.upload.status_code",
          FT_UINT16,
          BASE_DEC,
          VALS(a615a_op_status_codes),
          0x0,
          "A615a Part Load Op Status Code",
          HFILL}},
        {&hf_a615a_load_ratio,
         {"Load Ratio",
          "a615a.load_ratio",
          FT_STRINGZ,
          BASE_NONE,
          NULL,
          0x0,
          "A615a Load Operation Ratio",
          HFILL}},
        {&hf_a615a_file_count,
         {"File Count",
          "a615a.file_count",
          FT_UINT16,
          BASE_DEC,
          NULL,
          0x0,
          "A615a File Count",
          HFILL}},
        {&hf_a615a_file_name_length,
         {"File Name Length",
          "a615a.file_name.length",
          FT_UINT8,
          BASE_DEC,
          NULL,
          0x0,
          "A615a File Name Length",
          HFILL}},
        {&hf_a615a_file_name,
         {"File Name",
          "a615a.file_name",
          FT_STRINGZ,
          BASE_NONE,
          NULL,
          0x0,
          "A615a File Name",
          HFILL}},

        {&hf_a615a_file_description_length,
         {"File Description Length",
          "a615a.file_description.length",
          FT_UINT8,
          BASE_DEC,
          NULL,
          0x0,
          "A615a File Description Length",
          HFILL}},
        {&hf_a615a_file_description,
         {"File Description",
          "a615a.file_description",
          FT_STRINGZ,
          BASE_NONE,
          NULL,
          0x0,
          "A615a File Description",
          HFILL}},

        {&hf_a615a_part_number_length,
         {"Part Number Length",
          "a615a.part_number.length",
          FT_UINT8,
          BASE_DEC,
          NULL,
          0x0,
          "A615a Part Number Length",
          HFILL}},
        {&hf_a615a_part_number,
         {"Part Number",
          "a615a.part_number",
          FT_STRINGZ,
          BASE_NONE,
          NULL,
          0x0,
          "A615a Part Number",
          HFILL}},
        {&hf_a615a_tgt_hw_count,
         {"Number of Target Hardware",
          "a615a.num_hardware",
          FT_UINT16,
          BASE_DEC,
          NULL,
          0x0,
          "A615a Number of Target Hardware",
          HFILL}},
        {&hf_a615a_lit_name_length,
         {"Literal Name Length",
          "a615a.literal_name.length",
          FT_UINT8,
          BASE_DEC,
          NULL,
          0x0,
          "A615a Literal Name Length",
          HFILL}},
        {&hf_a615a_lit_name,
         {"Literal Name",
          "a615a.literal_name",
          FT_STRINGZ,
          BASE_NONE,
          NULL,
          0x0,
          "A615a Literal Name",
          HFILL}},
        {&hf_a615a_serial_num_length,
         {"Serial Number Length",
          "a615a.serial_number.length",
          FT_UINT8,
          BASE_DEC,
          NULL,
          0x0,
          "A615a Serial Number Length",
          HFILL}},
        {&hf_a615a_serial_num,
         {"Serial Number",
          "a615a.serial_number",
          FT_STRINGZ,
          BASE_NONE,
          NULL,
          0x0,
          "A615a Serial Number",
          HFILL}},
        {&hf_a615a_part_num_count,
         {"Part Number Count",
          "a615a.num_parts",
          FT_UINT16,
          BASE_DEC,
          NULL,
          0x0,
          "A615a Part Number Count",
          HFILL}},
        {&hf_a615a_ammendment_len,
         {"Ammendment Length",
          "a615a.ammendment.length",
          FT_UINT8,
          BASE_DEC,
          NULL,
          0x0,
          "A615a Ammendment Length",
          HFILL}},
        {&hf_a615a_ammendment,
         {"Ammendment",
          "a615a.ammendment",
          FT_STRINGZ,
          BASE_NONE,
          NULL,
          0x0,
          "A615a Ammendment",
          HFILL}},
        {&hf_a615a_designation_len,
         {"Designation Length",
          "a615a.designation.length",
          FT_UINT8,
          BASE_DEC,
          NULL,
          0x0,
          "A615a Designation Length",
          HFILL}},
        {&hf_a615a_designation,
         {"Designation",
          "a615a.designation",
          FT_STRINGZ,
          BASE_NONE,
          NULL,
          0x0,
          "A615a Designation",
          HFILL}},
        {&hf_a615a_user_data_len,
         {"User Data Length",
          "a615a.user_data.length",
          FT_UINT8,
          BASE_DEC,
          NULL,
          0x0,
          "User Data Length",
          HFILL}},
        {&hf_a615a_user_data,
         {"User Data",
          "a615a.user_data",
          FT_BYTES,
          BASE_NONE,
          NULL,
          0x0,
          "User Data",
          HFILL}},

        /* reassembly fields */
        {&hf_a615a_fragments, {"Message fragments", "a615a.fragments", FT_NONE, BASE_NONE, NULL, 0x00, NULL, HFILL}},
        {&hf_a615a_fragment, {"Message fragment", "a615a.fragment", FT_FRAMENUM, BASE_NONE, NULL, 0x00, NULL, HFILL}},
        {&hf_a615a_fragment_overlap, {"Message fragment overlap", "a615a.fragment.overlap", FT_BOOLEAN, 0, NULL, 0x00, NULL, HFILL}},
        {&hf_a615a_fragment_overlap_conflicts, {"Message fragment overlapping with conflicting data", "a615a.fragment.overlap.conflicts", FT_BOOLEAN, 0, NULL, 0x00, NULL, HFILL}},
        {&hf_a615a_fragment_multiple_tails, {"Message has multiple tail fragments", "a615a.fragment.multiple_tails", FT_BOOLEAN, 0, NULL, 0x00, NULL, HFILL}},
        {&hf_a615a_fragment_too_long_fragment, {"Message fragment too long", "a615a.fragment.too_long_fragment", FT_BOOLEAN, 0, NULL, 0x00, NULL, HFILL}},
        {&hf_a615a_fragment_error, {"Message defragmentation error", "a615a.fragment.error", FT_FRAMENUM, BASE_NONE, NULL, 0x00, NULL, HFILL}},
        {&hf_a615a_fragment_count, {"Message fragment count", "a615a.fragment.count", FT_UINT32, BASE_DEC, NULL, 0x00, NULL, HFILL}},
        {&hf_a615a_reassembled_in, {"Reassembled in", "a615a.reassembled.in", FT_FRAMENUM, BASE_NONE, NULL, 0x00, NULL, HFILL}},
        {&hf_a615a_reassembled_length, {"Reassembled length", "a615a.reassembled.length", FT_UINT32, BASE_DEC, NULL, 0x00, NULL, HFILL}},
    };

    /* setup protocol subtree array */
    static gint *ett[] = {
        &ett_a615a,
        &ett_a615a_opt_root,
        &ett_a615a_opt,
        &ett_a615a_fragment,
        &ett_a615a_fragments,
        &ett_a615a_protocol_root,
        &ett_a665_protocol_root

    };
    proto_a615a = proto_register_protocol(
        "Arinc 615a Protocol", /* name       */
        "A615a",               /* short name */
        "a615a"                /* abbrev     */
    );

    proto_register_field_array(proto_a615a, hf, array_length(hf));
    proto_register_subtree_array(ett, array_length(ett));
    reassembly_table_register(&a615a_reassembly_table, &addresses_ports_reassembly_table_functions);
    module_t *module = prefs_register_protocol(proto_a615a, NULL);

    /* a preference entry for the user to provide the data load server ports expected in the capture */
    prefs_register_range_preference(module, "load_server_control_ports",
                                    "A615a Load Server Control Ports",
                                    "Comma separated list of A615a data load server control ports.",
                                    &global_a615a_server_control_port_range,
                                    65535);

    /* a preference entry for the user to provide the load target control ports expected in the capture */
    prefs_register_range_preference(module, "load_target_control_ports",
                                    "A615a Load Target Control Ports",
                                    "Comma separated list of A615a data load target control ports.",
                                    &global_a615a_target_control_port_range,
                                    65535);
}

void proto_reg_handoff_a615a(void)
{
    a615a_handle = create_dissector_handle(dissect_a615a, proto_a615a);
    dissector_add_uint_range_with_preference("udp.port", "30000-65000", a615a_handle);
}