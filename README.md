## Arinc 615a Wireshark Dissector ##

    Arinc 615a Wireshark Dissector
    Copyright (C) 2019  Alex Rhodes
    https://www.alexrhodes.io

This is a dissector for the Arinc 615a data loader protocol. 

## Status
**This plugin requires a modification to the Wireshark code base that will be present in release versions >= 2.6.3**

This project is still under development. I am tracking bugs/TODOs/Progress here. It is likely to contain bugs and require improvements. I appreciate suggestions.

#### Builds:
The `builds` directory of this repository contains pre-built version(s) of this dissector plugin DLL based on the latest code. These may cause antivirus warnings etc. so it's probably preferable to build from source directly:

* `Win_8_x64_Wireshark_3.1_A615a_Plugin_DLL` - the DLL for this dissector, to be placed in the plugins directory of an existing installation. Built on 64 bit Windows 8 for Wireshark v3.1

#### TODO:
1. User's Guide/Instructions (see below build instructions)
2. Wireshark submission

#### Known Bugs:
1. Arinc 615a specifies that the TFTP block number rolls over to "1" when uint16 max blocks are reached. This throws off my reassembly. This shouldn't matter much because I don't dissect LUP files and those are the only files I expect to exceed the maximum block count. This is a _might_ fix.

#### Protocol dissection:

    Implemented:
    .LCI Load Configuration Initialization
    .LCL Load Configuration List
    .LCS Load Configuration Status
    .LNA Load Download Answer
    .LND Load Download Disk
    .LNL Load Download List
    .LNO Load Download Operator
    .LNR Load Download Request
    .LNS Load Download Status
    .LUI Load Upload Initialization
    .LUS Load Upload Status
    .LUR Load Upload Request

    665 Protocol Files Not Implemented (nice-to-have, probably not going to do them):
    .LUB Load Upload Batch: Defined by ARINC Report 665
    .LUH Load Upload Header: Defined by ARINC Report 665.
    .LUM Load Upload Media: Defined by ARINC Report 665.
    .LUP Load Upload Part (Data File): Defined by ARINC Report 665.

## Build Instructions

1. Follow the [Wireshark build/environment instructions](https://www.wireshark.org/docs/wsdg_html_chunked/ChSetupWin32.html) to get a base build running.
2. Add the code from this repo to a directory in `plugins/epan/` eg: `plugins/epan/a615a`
3. Rename/copy the `CMakeListsCustom.txt.example` in the wireshark root directory to `CMakeListsCustom.txt` and modify with the new plugin:
        
        # Fail CMake stage if any of these plugins are missing from source tree
        set(CUSTOM_PLUGIN_SRC_DIR
        #	private_plugins/foo
        # or
            plugins/epan/a615a
        )

4. Build as normal.

## Instructions
 
I will write up thorough/accurate instructions once the plugin is closer to being finalized. For now:

1. Disable TFTP or set the TFTP port range to `0` in Wireshark's preferences.
2. Set the A615a UDP port range to the required values expected in your trace. (for example: `3500-65000`)
3. Set the A615a load server port range to the data loader ports expected in your trace. (for example: `25100`)
4. Set the A615a load target port range to the load target control ports expected in your trace. (for example: `59,51100,51200,51300`)
3. See the available fields/filters by typing `a615a.` in the filter bar. (these need improvement)

**If your packets do not display as A615a, check your port range. If there are a lot of "incomplete file" labels, check your control ports. Anything else is probably a bug.**
